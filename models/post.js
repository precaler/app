const db = require('../util/database');

module.exports = class Post {
  constructor(title, body, categories, user) {
    this.title = title;
    this.body = body;
    this.categories = categories;
    this.user = user;
    
 
 
  }

  static fetchAll() {
    return db.execute('SELECT * FROM posts ORDER BY created DESC');
  }

  static save(post) {
    return db.execute(
      'INSERT INTO posts (title, body, categories, ID_user) VALUES (?, ?, ?, ?)',
      [post.title, post.body, post.categories, post.user]
    );
  }

  static delete(id) {
    return db.execute('DELETE FROM posts WHERE ID_post = ?', [id]);
  }
};
