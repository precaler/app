const db = require('../util/database');

module.exports = class ContactPlayer {
  constructor(email, name, lastName, age, height, weight, city, phone) {
    this.email = email;
    this.name = name;
    this.lastName = lastName;
    this.age = age;
    this.height = height;
    this.weight = weight;
    this.city = city;
    this.phone = phone;
    
 
 
  }

  static fetchAll() {
    return db.execute('SELECT * FROM contact_player ORDER BY created DESC');
  }

  static save(player) {
    return db.execute(
      'INSERT INTO contact_player (email, name, lastName, age, height, weight, city, phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
      [player.email, player.name, player.lastName, player.age, player.height,  player.weight,  player.city,  player.phone]
    );
  }

};
